import json
import strutils

# Créer un dictionnaire de gammes de musique
let gammes = parseJson("""{
  "Gamme majeure": {
    "intervalles": [2, 2, 1, 2, 2, 2, 1],
    "tons": 5,
    "demi-tons": 2
  },
  "Gamme mineure naturelle": {
    "intervalles": [2, 1, 2, 2, 1, 2, 2],
    "tons": 4,
    "demi-tons": 3
  },
  "Gamme mineure harmonique": {
    "intervalles": [2, 1, 2, 2, 1, 3, 1],
    "tons": 4,
    "demi-tons": 3
  },
}
""")

type
  Intervalle = seq[int]
  
# Fonction pour générer la liste des notes d'une gamme
proc genererNotesGamme(nomGamme: string, tonique: string): seq[string] =
  # Récupérer les informations de la gamme
  let gamme = gammes[nomGamme]
  # Récupérer les intervalles de la gamme
  let intervalles = to(gamme["intervalles"], Intervalle)
  # Créer un dictionnaire de correspondance entre les notes en bémol et les autres notes
  var correspondanceNotesBemol = [("Ab", "G#"), ("Bb", "A#"), ("Cb", "B"), ("Db", "C#"), ("Eb", "D#"), ("Fb", "E"), ("Gb", "F#")]
  # Convertir la tonique en dièse si nécessaire
  var toniqueDiese = ""
  var isToniqueBemol = false
  if tonique.endsWith("b"):
    for x in correspondanceNotesBemol:
      if x[0] == tonique:
        isToniqueBemol = true
        toniqueDiese = x[1]
        break
  else:
    toniqueDiese = tonique
  # Initialiser la liste des notes
  var notes = @[toniqueDiese]
  # Trouver l'index de la tonique dans l'échelle chromatique
  const echelleChromatique = @["C", "C#", "D", "D#", "E", "F", "F#","G", "G#", "A", "A#", "B"]
  let indexTonique = echelleChromatique.find(tonique)
  # Parcourir les intervalles de la gamme
  for intervalle in intervalles:
    # Ajouter l'intervalle à l'index de la dernière note de la liste
    var indexDerniereNote = echelleChromatique.find(notes[^1])
    var indexNouvelleNote = (indexDerniereNote + intervalle) mod 12
    # Ajouter la nouvelle note à la liste
    notes.add(echelleChromatique[indexNouvelleNote])
    # Convertir les notes en bémol si nécessaire
  for i in 0..<notes.len:
    if isToniqueBemol == true and notes[i].endsWith("#"):
      for x in correspondanceNotesBemol:
        if x[1] == notes[i]:
          notes[i] = x[0]
          break
  return notes

# Exemple d'utilisation de la fonction
var notesGammeMajeureC = genererNotesGamme("Gamme majeure", "G")
var notesGammeMajeureAb = genererNotesGamme("Gamme majeure", "Ab")
echo $notesGammeMajeureC
echo $notesGammeMajeureAb
